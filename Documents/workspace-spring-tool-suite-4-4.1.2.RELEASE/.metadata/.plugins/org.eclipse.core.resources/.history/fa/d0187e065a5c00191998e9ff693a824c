package com.bizzdesk.inventory.servce.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bizzdesk.inventory.constant.AppConstants;
import com.bizzdesk.inventory.constant.ServerResponseStatus;
import com.bizzdesk.inventory.dto.ActivateUserRequest;
import com.bizzdesk.inventory.dto.PasswordResetDto;
import com.bizzdesk.inventory.dto.ResendUserActivationCodeDto;
import com.bizzdesk.inventory.dto.ServerResponse;
import com.bizzdesk.inventory.dto.SignInRequest;
import com.bizzdesk.inventory.dto.SignUpRequest;
import com.bizzdesk.inventory.enunType.TokenType;
import com.bizzdesk.inventory.model.Role;
import com.bizzdesk.inventory.model.Token;
import com.bizzdesk.inventory.model.Users;
import com.bizzdesk.inventory.repository.RoleRepository;
import com.bizzdesk.inventory.repository.UsersRepository;
import com.bizzdesk.inventory.service.TokenService;
import com.bizzdesk.inventory.service.UsersService;
import com.bizzdesk.inventory.utility.Utility;


@Service
@Transactional
public class UsersServiceImpl implements UsersService{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	TokenService tokenService;
	
	
	@Autowired
	AppConstants appConstants;
	
	//@Autowired
	//private EmailService emailService;
	
    
    Utility utility = new Utility();
	
	
	
	@Override
	public Collection<Users> findAll() {
		
		try {
			return (Collection<Users>) usersRepository.findAll();
					
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return null;
	}

	@Override
	public Users findById(long id) {
		
		try {
			return usersRepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Users findByEmail(String emailAddress) {
		
		try {
			return usersRepository.findByEmailAddress(emailAddress);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Users findByPhone(String phoneNumber) {
		
		try {
			return usersRepository.findByPhoneNumber(phoneNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	
	@Override
	public Users findByEmailOrPhone(String emailAddress, String phoneNumber) {
		
		try {
			return usersRepository.findByPhoneNumberOrEmailAddress(phoneNumber, emailAddress);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}


/************************************************************************************************************
 *                            USER ACCOUNT CREATION
 ***********************************************************************************************************/
	
	@Override
	public ServerResponse create(SignUpRequest request) {
		
		
       ServerResponse response = new ServerResponse();
		
		Users users = null;
		
		String emailAddress = request.getEmailAddress() != null ? request.getEmailAddress() : request.getEmailAddress();
		String phoneNumber = request.getPhoneNumber() != null ? request.getPhoneNumber() : request.getPhoneNumber();;
		String firstName = request.getFirstName() != null ? request.getFirstName() : request.getFirstName();
		String lastName = request.getLastName() != null ? request.getLastName() : request.getLastName();
		
		if (emailAddress == null && !Utility.isValidEmail(emailAddress)) {
			
			response.setData("");
            response.setMessage("Please enter valid email address");
            response.setSuccess(false);

            return response;
		}
		
		if (phoneNumber == null || !Utility.isValidPhone(phoneNumber)) {
			response.setData("");
            response.setMessage("Please enter valid phone number");
            response.setSuccess(false);
            response.setStatus(ServerResponseStatus.FAILED);

            return response;
		}
		
		if (firstName == null) {
			response.setData("");
            response.setMessage("Please enter first name");
            response.setSuccess(false);
            response.setStatus(ServerResponseStatus.FAILED);

            return response;
		}
		
		try {
			
			Users requestUsersEmail = usersRepository.findByEmailAddress(emailAddress);
					
			
			if (requestUsersEmail != null) {
				response.setData("");
                response.setMessage("Email already exist");
                response.setSuccess(false);
                response.setStatus(ServerResponseStatus.FAILED);

                return response;
			}
			
			Users requestUser = usersRepository.findByPhoneNumberOrEmailAddress(phoneNumber, emailAddress);
			
			if (requestUser != null) {
				response.setData("");
                response.setMessage("User email or number already exist");
                response.setSuccess(false);
                response.setStatus(ServerResponseStatus.FAILED);

                return response;
			}
			
			users = new Users();
			
			Token supportCode = tokenService.generate(TokenType.ACCOUNT_CODE);
			supportCode.setUsers(users);
						
			Role roleCode = roleRepository.findByName("USER");
			
			if(roleCode == null){
				response.setData("");
                response.setMessage("Invalid user role");
                response.setSuccess(false);
                response.setStatus(ServerResponseStatus.FAILED);

                return response;
			}
		
			//logger.info(roleCode.getId() + " role " + roleCode.getName());
			String activationCode = String.valueOf(Utility.generateActivationCode());

			Role role = new Role();
			role = roleCode;
			users.setRole(role);
			users.setEmailAddress(emailAddress);
			users.setFirstName(firstName);
			users.setLastName(lastName);
			users.setPhoneNumber(phoneNumber);
			users.setDateCreated(new Date());
			users.setActive(false);
			users.setActivationCode(activationCode);
		
			entityManager.persist(users);
			
            entityManager.close();
            
//            Mail mail = new Mail();
//            mail.setTo(email);
//            mail.setFrom("habeexdev@gmail.com");
//            mail.setSubject("Account verification");

            Map<String, Object> model = new HashMap<String, Object>();
            model.put("name", firstName);
            model.put("code", activationCode);
          //  mail.setModel(model);
          //  mail.setTemplate("account_verification_email_template.ftl");
         //   emailService.sendSimpleMessage(mail);
            
	        response.setData(users);
            response.setMessage("User successfully created");
            response.setSuccess(true);
            response.setStatus(ServerResponseStatus.OK);
            

		} catch (Exception e) {
		  response.setData("");
          response.setMessage("Failed to create user account");
          response.setSuccess(false);
          response.setStatus(ServerResponseStatus.FAILED);

       //   logger.error("An error occured while creating recipient account");
          e.printStackTrace();
		}
		return response;
		
	}



     @Override
      public ServerResponse reSendUserActivation(ResendUserActivationCodeDto request) {
	      // TODO Auto-generated method stub
	    return null;
         }

     @Override
      public ServerResponse reSendUserPassword(ResendUserActivationCodeDto request) {
        	// TODO Auto-generated method stub
	     return null;
         }

     @Override
      public ServerResponse passwordReset(PasswordResetDto request) {
	    // TODO Auto-generated method stub
	     return null;
        }

     @Override
     public ServerResponse login(SignInRequest request) {
	    // TODO Auto-generated method stub
	   return null;
      }

     @Override
     public ServerResponse userActivation(ActivateUserRequest request) {
	   // TODO Auto-generated method stub
	   return null;
      }

		
	
}
