package com.francis.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Beginning of the code
@SpringBootApplication
public class IntegrationApplication {

	//main program
	public static void main(String[] args) {
		SpringApplication.run(IntegrationApplication.class, args);
	}

}
